import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { ResultWidgetComponent } from './result-widget/result-widget.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastComponent } from './toast/toast.component';
import { FeildErrorDisplayComponent } from './feild-error-display/feild-error-display.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerFormComponent,
    ResultWidgetComponent,
    ToastComponent,
    FeildErrorDisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ResultWidgetComponent,
    ToastComponent
  ]
})
export class AppModule { }
