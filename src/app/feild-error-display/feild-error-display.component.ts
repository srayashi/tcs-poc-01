import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-feild-error-display',
  templateUrl: './feild-error-display.component.html',
  styleUrls: ['./feild-error-display.component.css']
})
export class FeildErrorDisplayComponent implements OnInit {
  @Input() errorMsg: string | undefined;
  @Input() displayError: boolean | undefined;
  constructor() { }

  ngOnInit(): void {
  }

}
