import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeildErrorDisplayComponent } from './feild-error-display.component';

describe('FeildErrorDisplayComponent', () => {
  let component: FeildErrorDisplayComponent;
  let fixture: ComponentFixture<FeildErrorDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeildErrorDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeildErrorDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
