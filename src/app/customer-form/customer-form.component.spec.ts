import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomerFormComponent } from './customer-form.component';

describe('CustomerFormComponent', () => {
  let component: CustomerFormComponent;
  let fixture: ComponentFixture<CustomerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerFormComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should check user email is invalid', () => {
    let email = component.signupForm.controls['email'];
    expect(email.valid).toBeFalsy();
    expect(email.touched).toBeFalsy();
    expect(email.errors['required']).toBeTruthy();
    email.setValue('abc');
    expect(email.errors['email']).toBeTruthy();
  });
  it('should check user email is in valid format', () => {
    let email = component.signupForm.controls['email'];
    email.setValue('abc@gmail.com');
    expect(email.errors).toBeNull();
  });
  it('should check user password is in valid format', () => {
    let password = component.signupForm.controls['password'];
    expect(password.errors['required']).toBeTruthy();
    password.setValue('aA1');
    expect(password.errors).toBeNull();
  });
  it('should check user age is invalid', () => {
    let age = component.signupForm.controls['age'];
    expect(age.valid).toBeFalsy();
    expect(age.touched).toBeFalsy();
    expect(age.errors['required']).toBeTruthy();
    age.setValue('20-29');
    expect(age.errors).toBeNull();
  });
  it('should check user rating is invalid', () => {
    let rating = component.signupForm.controls['rating'];
    expect(rating.valid).toBeFalsy();
    expect(rating.touched).toBeFalsy();
    expect(rating.errors['required']).toBeTruthy();
    rating.setValue('1');
    expect(rating.errors).toBeNull();
  });
  it('should check user rating comment is invalid', () => {
    let rating_comment = component.signupForm.controls['rating_comment'];
    rating_comment.setValue('aaa');
    expect(rating_comment.errors).toBeNull();
  });
  it('should check Form is invalid', () => {
    expect(component.signupForm.valid).toBeFalsy();
  });
  it('should check Form is invalid or not after enter the value', () => {
    component.signupForm.controls['rating'].setValue('1');
    component.signupForm.controls['rating_comment'].setValue(null);
    component.signupForm.controls['age'].setValue('20-29');
    component.signupForm.controls['email'].setValue('abc@gmail.com');
    component.signupForm.controls['password'].setValue('aA1');
    expect(component.signupForm.valid).toBeTruthy();
  });
  it('should check Form is submitted or not after enter the value', () => {
    component.signupForm.controls['rating'].setValue('1');
    component.signupForm.controls['rating_comment'].setValue(null);
    component.signupForm.controls['age'].setValue('20-29');
    component.signupForm.controls['email'].setValue('abc@gmail.com');
    component.signupForm.controls['password'].setValue('aA1');
    fixture.detectChanges();
    component.onSubmit();
    component.creatComponent(true, component.signupForm.valid ? 'success': 'error')
  });
  it('should check changeAge', () => {
    fixture.detectChanges();
    component.changeAge(component.signupForm.controls['age'].setValue('20-29'));
    component.signupForm.controls['age'].setValue('20-29');
    expect(component.selectedAge).toEqual(component.signupForm.controls['age'].setValue('20-29'));
  });
  it('should check changeRating', () => {
    fixture.detectChanges();
    component.signupForm.controls['rating'].setValue('1');
    expect(component.selectedRating).toEqual(component.signupForm.controls['rating'].setValue('1'));
  });
});
