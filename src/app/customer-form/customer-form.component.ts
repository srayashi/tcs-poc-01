import { isNull } from '@angular/compiler/src/output/output_ast';
import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AbstractControl, FormArray,FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ResultWidgetComponent } from '../result-widget/result-widget.component';
import { ToastComponent } from '../toast/toast.component';
import UsersJson from '../users.json';
interface Iinput {
  id: Number
  label: string
  type: string
  validation: Object
  sub_questions: Array<any>
}
interface Ainput {
  id: Number
  name: string
}
interface USERS {
  questions: Array<any>;
  ages: Array<any>;
}
interface tplotOptions {
  [key: string]: string
}
interface tplotOptions2 {
  [key: string]: boolean
}
@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})

export class CustomerFormComponent implements OnInit {
  customer: USERS = UsersJson;
  rating_numbers = Array(5).fill(0).map((x, i) => i + 1);
  isShow: tplotOptions2 = {
    'rating': false,
    'age': false,
    'password': false,
    'email': false
  };
  comment_place_holder: tplotOptions = {
    'rating': '',
    'age': '',
    'password': '',
    'email': ''
  };
  signupForm: any;
  selectedAge: any;
  selectedRating: any;
  age_sub_ques = [];
  @ViewChild('dynamicLoadComponent', { read: ViewContainerRef })
  entryContainer: ViewContainerRef | undefined;
  ComponentRef: any;
  constructor(private resolver: ComponentFactoryResolver, private _fb: FormBuilder) { }

  ngOnInit() {
    this.signupForm = this._fb.group({
      'rating': new FormControl(null),
      'rating_comment': new FormControl(null),
      'rating_sub': this._fb.array([]),
      'age': new FormControl(null),
      'age_sub': this._fb.array([]),
      'age_comment': new FormControl(null),
      'password': new FormControl(null),
      'password_sub': this._fb.array([]),
      'password_comment': new FormControl(null),
      'email': new FormControl(null),
      'email_sub': this._fb.array([]),
      'email_comment': new FormControl(null),
    });
    this.setValidators();
  }
  onSubmit() {
    console.log(this.signupForm.value)
    this.creatComponent(true, this.signupForm.valid ? 'success' : 'error');
    if (!this.signupForm.valid) {
      this.validateAllFormFields(this.signupForm);
    } else {
      this.signupForm.reset();
      this.selectedAge = '';
      this.selectedRating = '';
      this.isShow = {
        'rating': false,
        'age': false,
        'password': false,
        'email': false
      };
    }
  };
  patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): any => {
      const valid = regex.test(control.value);
      if (!valid) {
        return error;
      }
    };
  }
  changeAge(val: any) {
    this.selectedAge = val;
    this.signupForm.get('age').setValue(val);
    const sub_ques = this.getQuestion('age').sub_questions;
    this.ageArray.controls = [];
    if (sub_ques.length) {
      this.age_sub_ques = sub_ques[0].questions;
      this.getSubquestion(sub_ques, 'age');
      this.age_sub_ques.map((x)=> {
        this.ageArray.push(this.subGroup());
      })
    }
  }
  changeRating(val: any) {
    const sub_ques = this.getQuestion('rating').sub_questions.filter((x: { values: string | string[]; }) => {
      if (x.values.includes(JSON.stringify(val))) {
        return x;
      }
      return false;
    })
    this.creatComponent(false, this.customer.questions[0].label, val);
    if (sub_ques.length) {
      this.ratingArray.controls = [];
      this.getSubquestion(sub_ques, 'rating');
      sub_ques[0].questions.map((x:any)=> {
        this.ratingArray.push(this.subGroup());
      })
    }
    else {
      this.isShow.rating = false;
    }
    this.selectedRating = val;
    this.signupForm.get('rating').setValue(val);
  }
  creatComponent(isToast: boolean, label?: string, id?: Number) {
    this.entryContainer?.clear();
    if (!isToast) {
      const factory = this.resolver.resolveComponentFactory(ResultWidgetComponent);
      this.ComponentRef = this.entryContainer?.createComponent(factory);
      this.ComponentRef.instance.label = label;
      this.ComponentRef.instance.rating = id;
    } else {
      const factory = this.resolver.resolveComponentFactory(ToastComponent);
      this.ComponentRef = this.entryContainer?.createComponent(factory);
      this.ComponentRef.instance.label = label;
    }

  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  isFieldValid(field: string) {
    return !this.signupForm.get(field).valid && this.signupForm.get(field).touched;
  }
  setValidators(): void {
    this.customer.questions.map((x) => {
      if (x.type == 'rating' || 'age') {
        this.signupForm.controls[x.type].setValidators(x.validation.required ? [Validators.required] : null);;
        this.signupForm.controls[x.type].updateValueAndValidity();
      }
      if (x.type == 'email') {
        let valArr = [Validators.email]
        x.validation.required ? valArr.push(Validators.required) : valArr;
        this.signupForm.controls[x.type].setValidators(valArr);
        this.signupForm.controls[x.type].updateValueAndValidity();
      }
      if (x.type == 'password') {
        let valArr = [this.patternValidator(/\d/, { hasNumber: true }).bind(this),
        this.patternValidator(/[a-z]/, { hasSmallCase: false }).bind(this)]
        x.validation.required ? valArr.push(Validators.required) : valArr;
        this.signupForm.controls[x.type].setValidators(valArr);
        this.signupForm.controls[x.type].updateValueAndValidity();
      }
    })
  }
  getLabel(val: string) {
    return this.customer.questions.find(x => x.type === val).label;
  }
  getSubQuestion(val: string) {
    return this.customer.questions.find(x => x.type === val).sub_questions[0].questions;
  }
  getQuestion(val: string) {
    return this.customer.questions.find(x => x.type === val);
  }
  getSubquestion(sub_ques: any, key: string) {
    this.comment_place_holder[key] = sub_ques[0].questions[0].label;
    this.signupForm.controls[key + '_comment'].setValidators(sub_ques[0].questions[0].validation.required ? [Validators.required] : null);;
    this.signupForm.controls[key + '_comment'].updateValueAndValidity();
    this.isShow[key] = true;
  }
  checkFeildValidity(e: any, key: any) {
    if (this.signupForm.get(key).valid) {
      const sub_ques = this.getQuestion(key).sub_questions;
      if (sub_ques.length) {
        this.getSubquestion(sub_ques, key);
        if(key === 'email') {
          this.emailArray.push(this.subGroup());
        }
        if(key === 'password') {
          this.passwordArray.push(this.subGroup());
        }
      }
    }
  }
  private subGroup(): FormGroup {
    return this._fb.group({
      label: [],
    });
  }
  get ageArray(): FormArray {
    return <FormArray>this.signupForm.get('age_sub');
  }
  get ratingArray(): FormArray {
    return <FormArray>this.signupForm.get('rating_sub');
  }
  get emailArray(): FormArray {
    return <FormArray>this.signupForm.get('email_sub');
  }
  get passwordArray(): FormArray {
    return <FormArray>this.signupForm.get('password_sub');
  }
}
