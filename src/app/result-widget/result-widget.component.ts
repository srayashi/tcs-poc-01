import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-result-widget',
  templateUrl: './result-widget.component.html',
  styleUrls: ['./result-widget.component.css']
})
export class ResultWidgetComponent implements OnInit {
  label: string | undefined;
  rating: Number | undefined;
  today= new Date();
  constructor() { }

  ngOnInit(): void {
  }

}
